# Black-List
Black-List es un Software desarrollado en Java, para la gestion de información contenida en una base de datos en MySQL.
Black-List es software libre desarrolado por  m2; puede redistribuirlo y/omodificarlo bajo los término de la Licencia Pública General GNU tal como se publica por la Free Software Foundation; ya sea la versión 3 de la Licencia, o (a su elección) cualquier versión posterior.

Este programa se distribuye con la esperanza de que le sea útil, pero SIN NINGUNA GARANTÍA; La idea principal de este software es dar una idea de cómo utilizar los métodos para la conexión de Java con MySQL.

Ninguno de los métodos y clases utilizadas aquí son absolutas, cualquier desarrollador las puede modificar y/o eliminar si así lo considera.

# Las caraterísticas y funciones que incluye en mini-software son:

- Guardado de fechas en mysql
- Abrir link desde clic sobre JLabel.
- Guardado y recuperación de imágenes guardas en MySQL usando tipo de dato LONGBLOB.
- Recuperación de las fechas y colocadas en una tabla
- Colocado de fechas en un JDateChooser
- Guardado de contraseñas encriptadas en md5 en mysql
- Validación de usuarios a través de la contraseña encriptadas
- SplashScreen con barra de progreso modificable.
- Respaldar la base de datos
- Restaurar la base de datos (ojo, no se valida que el usuario esté en la bd después de respaldar)
- Modificar la información del usuario en configuración de la cuenta.
- Login.
- Creación de usuarios.
- Detección al presionar enter: es decir, que cuando se presiona enter en un cuadro de texto automáticamente se ejecuta la acción correspondiente.
- Altas, bajas, modificaciones, busquedas en la BD.
- Uso de JDialog.
- Búsqueda datos mientras se escribe: es decir, se van filtrando los registros al escribir.
- Detección de la tecla scape: es decir, en algunas ventanas, se detecta la tecla scape, para indicar que se cancela la edición o modificación.
- Uso de ícono para la aplicación.
- Deshabilitar la opción del botón cerrar.
- Uso de abrir y regresar a otro jframe.
- Uso de clases.
- Uso de objetos completos.
- Manipulación de objetos como registros.
- Incluye Script de la BD.

# Información Técnica
Este software se desarrolló bajo las siguientes características:

- Versión del Software: 2.3.0
- Versión de Java: 1.8.0_91
- Versión de JDK: jdk-8u91-linux-x64
- Versión de Netbeans: 8.1
- Sistema Operativo: Fedora 23 x86_64
- Version MySQL: 5.7.12 MySQL Community Server (GPL)

# Capturas

![alt tag](https://3.bp.blogspot.com/-h8k252rCMN4/V0Z3XLnMJFI/AAAAAAAACL8/Rn7AvtFerS0Uf4CFq6P1ofuNjAR1VAiUwCLcB/s1600/Captura%2Bde%2Bpantalla%2Bde%2B2016-05-25%2B10-39-04.png)

![alt tag](https://1.bp.blogspot.com/-L1SB_Jmp_QM/V0Z27PUSuuI/AAAAAAAACL0/5fapueNaMNI9If__AmfuS7gB3SPOb7_xACLcB/s1600/Captura%2Bde%2Bpantalla%2Bde%2B2016-05-24%2B19-19-52.png)

![alt tag](https://2.bp.blogspot.com/-HflTDFC3SQo/VypeRdvm82I/AAAAAAAAB7k/x2TPg6_MXFga8FgjGHmqBMondMKf58UaACLcB/s1600/Captura%2Bde%2Bpantalla%2Bde%2B2016-05-04%2B15-37-36.png)

![alt tag](https://1.bp.blogspot.com/-mqPCplcQHoI/VypeSRqSa5I/AAAAAAAAB7w/Rp_hSgwBM30f7QV5BcZngJp3c8W2UxfXwCLcB/s1600/Captura%2Bde%2Bpantalla%2Bde%2B2016-05-04%2B15-37-38.png)

![alt tag](https://4.bp.blogspot.com/-Xv2xvfk1OFE/VyqNVy6ZG-I/AAAAAAAAB88/9dk090176w0ZeINKta_aHXY-xY_6gdEEwCLcB/s1600/Captura%2Bde%2Bpantalla%2Bde%2B2016-05-04%2B18-55-24.png)
